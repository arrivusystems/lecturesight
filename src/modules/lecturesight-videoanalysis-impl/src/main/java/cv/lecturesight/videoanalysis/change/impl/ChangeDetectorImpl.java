/* Copyright (C) 2012 Benjamin Wulff
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
package cv.lecturesight.videoanalysis.change.impl;

import com.nativelibs4java.opencl.CLImage2D;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLQueue;
import cv.lecturesight.display.DisplayService;
import cv.lecturesight.videoanalysis.change.ChangeDetector;
import cv.lecturesight.framesource.FrameSource;
import cv.lecturesight.framesource.FrameSourceProvider;
import cv.lecturesight.opencl.OpenCLService;
import cv.lecturesight.opencl.OpenCLService.Format;
import cv.lecturesight.opencl.api.ComputationRun;
import cv.lecturesight.opencl.api.OCLSignal;
import cv.lecturesight.util.Log;
import cv.lecturesight.util.conf.Configuration;
import java.util.EnumMap;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;

/** Implementation of Service API
 *
 */
@Component(name="lecturesight.changedetector",immediate=true)
@Service
public class ChangeDetectorImpl implements ChangeDetector {

  Log log = new Log("Change Detector Service");
  @Reference
  Configuration config;
  @Reference
  private OpenCLService ocl;
  @Reference
  private DisplayService dsps;
  @Reference
  private FrameSourceProvider fsp;
  private FrameSource fsrc;
  CLImage2D input, last;
  CLImage2D changeMapRaw;
  CLImage2D changeMapDilated;
  private int[] workDim;
  private EnumMap<ChangeDetector.Signal, OCLSignal> signals =
          new EnumMap<ChangeDetector.Signal, OCLSignal>(ChangeDetector.Signal.class);

  protected void activate(ComponentContext cc) {
    // create signals
    signals.put(Signal.DONE_DETECTION, ocl.getSignal(Constants.SIGNAME_DONE_DETECTION));
    
    // set up input
    fsrc = fsp.getFrameSource();
    input = fsrc.getImage();
    workDim = new int[]{fsrc.getWidth(), fsrc.getHeight()};

    // allocate gpu buffers
    last = ocl.context().createImage2D(Usage.InputOutput, Format.BGRA_UINT8.getCLImageFormat(), workDim[0], workDim[1]);
    changeMapRaw = ocl.context().createImage2D(Usage.InputOutput, Format.INTENSITY_UINT8.getCLImageFormat(), workDim[0], workDim[1]);
    changeMapDilated = ocl.context().createImage2D(Usage.InputOutput, Format.INTENSITY_UINT8.getCLImageFormat(), workDim[0], workDim[1]);

    ocl.utils().copyImage(0, 0, workDim[0], workDim[1], input, 0, 0, last);

    registerDisplays();

    // register computation runs
    ocl.registerLaunch(fsrc.getSignal(), new ChangeDetectRun());
    log.info("Activated");
  }

  private void registerDisplays() {
    dsps.registerDisplay(Constants.WINDOWNAME_CHANGE_RAW, changeMapRaw, signals.get(Signal.DONE_DETECTION));
    dsps.registerDisplay(Constants.WINDOWNAME_CHANGE_DILATED, changeMapDilated, signals.get(Signal.DONE_DETECTION));
  }

  @Override
  public OCLSignal getSignal(Signal signal) {
    return signals.get(signal);
  }

  @Override
  public CLImage2D getChangeMapRaw() {
    return changeMapRaw;
  }

  @Override
  public CLImage2D getChangeMapDilated() {
    return changeMapDilated;
  }

  private class ChangeDetectRun implements ComputationRun {

    OCLSignal SIG_done = signals.get(Signal.DONE_DETECTION);
    CLKernel absDiffThreshK = ocl.programs().getKernel("change", "abs_diff_thresh");
    CLKernel dilateK = ocl.programs().getKernel("change", "image_dilate8");

    {
      dilateK.setArgs(changeMapRaw, changeMapDilated);
    }

    @Override
    public void launch(CLQueue queue) {
      absDiffThreshK.setArgs(input, last, changeMapRaw, config.getInt(Constants.PROPKEY_THRESH));
      absDiffThreshK.enqueueNDRange(queue, workDim);
      ocl.utils().copyImage(0, 0, workDim[0], workDim[1], input, 0, 0, last);
      dilateK.enqueueNDRange(queue, workDim);
    }

    @Override
    public void land() {
      ocl.castSignal(SIG_done);
    }
  }
}
