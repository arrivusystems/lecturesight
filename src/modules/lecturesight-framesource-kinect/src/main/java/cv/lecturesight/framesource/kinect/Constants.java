package cv.lecturesight.framesource.kinect;

public class Constants {

  final static int KINECT_IMAGE_WIDTH = 320;
  final static int KINECT_IMAGE_HEIGHT = 240;
}
