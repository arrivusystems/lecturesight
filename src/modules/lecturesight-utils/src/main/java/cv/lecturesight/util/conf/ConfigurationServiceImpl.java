/* Copyright (C) 2012 Benjamin Wulff
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
package cv.lecturesight.util.conf;

import cv.lecturesight.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class ConfigurationServiceImpl implements ConfigurationService {

  private Log log = new Log("System Configuration");
  Properties config;
  Properties defaults;
  List<ConfigurationListener> listeners = new LinkedList<ConfigurationListener>();

  public ConfigurationServiceImpl(Properties config, Properties defaults) {
    this.config = config;
    this.defaults = defaults;
  }

  @Override
  public Properties getSystemConfiguration() {
    return config;
  }
  
  @Override
  public Properties getSystemDefaults() {
    return defaults;
  }

  @Override
  public void loadSystemConfiguration(InputStream is) {
    try {
      config.load(is);
      log.info("Loaded system configuration.");
    } catch (IOException ex) {
      log.warn("Unable to read configuration: " + ex.getMessage());
      throw new RuntimeException("Unable to read configuration.", ex);
    }
  }

  @Override
  public void saveSystemConfiguration(OutputStream os) {
    try {
      config.store(os, " File generated by LectureSight Configuration Service ");
      log.info("Stored system configuration.");
    } catch (Exception e) {
      log.error("Unable to save configuration.", e);
      throw new RuntimeException("Unable to write configuration", e);
    }
  }

  @Override
  public void notifyListeners() {
    for (Iterator<ConfigurationListener> it = listeners.iterator(); it.hasNext(); ) {
      it.next().configurationChanged();
    }
  }
  
  @Override
  public void addConfigurationListener(ConfigurationListener l) {
    listeners.add(l);
  }

  @Override
  public void removeConfigurationListener(ConfigurationListener l) {
    listeners.remove(l);
  }
}
